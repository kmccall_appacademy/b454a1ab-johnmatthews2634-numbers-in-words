SINGLE_DIGIT_HASH = {
  0 => "zero",
  1 => "one",
  2 => "two",
  3 => "three",
  4 => "four",
  5 => "five",
  6 => "six",
  7 => "seven",
  8 => "eight",
  9 => "nine"
}

TEENS_HASH = {
  10 => "ten",
  11 => "eleven",
  12 => "twelve",
  13 => "thirteen",
  14 => "fourteen",
  15 => "fifteen",
  16 => "sixteen",
  17 => "seventeen",
  18 => "eighteen",
  19 => "nineteen"
}

TENS_HASH = {
  20 => "twenty",
  30 => "thirty",
  40 => "forty",
  50 => "fifty",
  60 => "sixty",
  70 => "seventy",
  80 => "eighty",
  90 => "ninety"
}
MAGNITUDES = {
  100 => "hundred",
  1000 => "thousand",
  1000000 => "million",
  1000000000 => "billion",
  1000000000000 => "trillion"
}

class Fixnum
  def in_words
    if (self < 10)
      SINGLE_DIGIT_HASH[self]
    elsif (self < 20)
      TEENS_HASH[self]
    elsif (self < 100)
      if (self % 10) != 0
        TENS_HASH[(self / 10) * 10] + " " + SINGLE_DIGIT_HASH[self - (self / 10) * 10]
      else
        TENS_HASH[self]
      end
    else
     magnitude = find_magnitude
     magnitude_words =
       (self / magnitude).in_words + " " + MAGNITUDES[magnitude]
     if (self % magnitude) != 0
       magnitude_words + " " + (self % magnitude).in_words
     else
       magnitude_words
     end
   end
 end

 def find_magnitude
   MAGNITUDES.keys.take_while { |magnitude| magnitude <= self }.last
 end
end
